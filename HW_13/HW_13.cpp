#include <iostream>
#include "Helpers.h"

int squareSum(int a, int b)
{
	int sum = a + b;
	int result = sum * sum;

	return result;
}

int main()
{
    std::cout << squareSum(2, 2);
}

