#include <iostream>
#include <string>

int main()
{
    std::string myString = "Hello World!";

    std::cout << myString << std::endl;
    std::cout << myString.size() << std::endl;
    std::cout << myString.front() << std::endl;
    std::cout << myString.back() << std::endl;

    return 0;
}